package reminders

import (
	"embed"
)

// Migrations represents the migrations folder
//go:embed migrations
var Migrations embed.FS

// UI represents the application user interface
//go:embed ui/dist
var UI embed.FS

// Email represents the email templates
//go:embed email_templates
var EmailTemplates embed.FS
