# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.6.0] - 19/11/2022
### Added
- #20 Exposing key RED metrics via the `/metrics` endpoint in Prometheus format;

  - `http_errors_total` partitioned by status code,.
  - `http_request_duration_seconds` partitioned by path and method.
  - `http_requests_total` partitioned by path, method and status.

### Changed
- #22 Updated dependencies.

## [1.5.0] - 11/09/2022
### Changed
- #17 Container now runs as a non-root user with UID 10000 and GID 10001.

### Added
- #15 Included support for developing in a devcontainer.

## [1.4.0] - 11/06/2022
### Changed
- #14 Reconfigured migrations to strip line breaks in order to ensure that the resulting hash is the same on Windows and Linux.  This allows for porting between the two, which may be unlikely, but without this change running the migrations would fail.

  **Important:** To support this change you must delete all records from the migrations table.  This will force migrations to run again, which are idempotent from a schema perspective, although be aware that all user records will be deleted.  That doesn't really matter, it will just force users to log in again.

## [1.3.0] - 05/06/2022
### Changed
- #13 Require UPN for logging in instead of username.  Facilitates support for multiple domains, although authentication for multiple domains is not currently enabled.

## [1.2.1] - 31/05/2022
### Fixed
- #12 An error in the `tailwind.config.js` file meant that there was no UI styling at all; Tailwind purged all of the styles due to an incorrect glob path.

## [1.2.0] - 30/05/2022
### Changed
- #11 Updated all frontend and backend dependencies to their latest versions.

## [1.1.0] - 02/05/2022
### Changed
- Updated all frontend and backend dependencies to latest versions.
- #10 Retry up to 10 times, with exponential backoff, if connection to SMTP server fails.

## [1.0.0] - 15/12/2021
- Production ready release

## [0.1.7] - 10/12/2021
### Changed
- Improved wording on reminder acknowledgment page.

## [0.1.6] - 10/12/2021
### Changed
- #7 Improved the reminder acknowledgment view by stating what is going to happen to the reminder; either it is going to be deleted because it is non-recurring or the notification date will be reset.

## [0.1.5] - 10/12/2021
### Fixed
- #6 Fixed typo in debug logging.

## [0.1.4] - 10/12/2021
### Changed
- #5 Hid debug logging behind a debug flag.  It will now only print if you set the `reminders_debug` environment variable.

## [0.1.3] - 09/12/2021
### Changed
- #4 Additional logging to debug the sending of reminders.

## [0.1.2] - 09/12/2021
### Fixed
- #3 Corrected naming for `cron_interval` flag.  It was missing the `v`.

## [0.1.1] - 09/12/2021
### Added
- #2 New `cron_interval` flag to allow for a custom interval to be set for processing reminders.  The default is `0 3 * * *` which is every day at 03:00.

## [0.1.0] - 07/12/2021
- Initial test release of rewritten reminders application.  Original was baked into another application but has now been split out.
