FROM golang:alpine as builder

ENV VERSION="1.6.0"

ARG UID=10000
ARG GID=10001

WORKDIR $GOPATH/src/github.com/rokett
RUN \
    apk add --no-cache git nodejs npm && \
    addgroup --g $GID remindersd && \
    adduser --disabled-password --no-create-home --shell /sbin/nologin --uid $UID --ingroup remindersd remindersd && \
    git clone --branch $VERSION --depth 1 https://gitlab.com/rokett/reminders.git reminders && \
    cd reminders && \
    chmod -R 777 ./node_modules && \
    npx tailwindcss -i ./ui/src/app.css -o ./ui/tmp/app.css && \
    npx cleancss -o ./ui/tmp/app.min.css ./ui/tmp/app.css && \
    npm run build && \
    BUILD=$(git rev-list -1 HEAD) && \
    CGO_ENABLED=0 GOOS=linux go build -a -mod=vendor -ldflags "-X main.version=$VERSION -X main.build=$BUILD -s -w -extldflags '-static'" -o remindersd ./cmd/remindersd

FROM scratch
LABEL maintainer="rokett@rokett.me"
COPY --from=builder /go/src/github.com/rokett/reminders/remindersd /
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Run with non-root user
USER remindersd

EXPOSE 4000

ENTRYPOINT ["./remindersd"]
