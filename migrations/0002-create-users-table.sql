IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'users')
    CREATE TABLE users (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        username NVARCHAR(25) NOT NULL,
        token NVARCHAR(40),
        token_expiry DATETIME2(3),
        token_max_age NVARCHAR(255),
        last_login DATETIME2(3),
        created_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'users_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER users_updated_at ON users FOR UPDATE AS
        UPDATE users
        SET updated_at = GETUTCDATE()
        FROM users INNER JOIN deleted d
        ON users.id = d.id;');
