IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'reminders')
	CREATE TABLE reminders (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        title NVARCHAR(250) NOT NULL,
        [description] NVARCHAR(max),
        frequency INT NOT NULL,
        frequency_unit NVARCHAR(15) NOT NULL,
        start_date DATETIME2(3) NOT NULL,
        notification_date DATETIME2(3) NOT NULL,
        notification_email_addresses NVARCHAR(500) NOT NULL,
        recurring bit NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'reminders_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER reminders_updated_at ON reminders FOR UPDATE AS
        UPDATE reminders
        SET updated_at = GETDATE()
        FROM reminders INNER JOIN deleted d
        ON reminders.id = d.id;');
