package main

import (
	"html/template"
	"io/fs"
	"net/url"
	"path/filepath"
	"time"

	"rokett.me/reminders"
	"rokett.me/reminders/internal/models"
)

type templateData struct {
	Reminders         []models.ReminderView
	FormData          url.Values
	FormErrors        map[string]string
	Flash             string
	Action            string
	RecurringReminder bool
	IsAuthenticated   bool
}

func newTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := fs.Glob(reminders.UI, "ui/dist/html/*.page.html")
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.New(name).Funcs(functions).ParseFS(reminders.UI, page)
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseFS(reminders.UI, "ui/dist/html/*.layout.html")
		if err != nil {
			return nil, err
		}

		//ts, err = ts.ParseFS(reminders.UI, "ui/dist/html/*.partial.html")
		//if err != nil {
		//	return nil, err
		//}

		cache[name] = ts
	}

	return cache, nil
}

func friendlydate(t time.Time) string {
	return t.Format("02 Jan 2006")
}

func afterEpoch(t time.Time) bool {
	return t.After(time.Time{})
}

var functions = template.FuncMap{
	"friendlydate": friendlydate,
	"afterEpoch":   afterEpoch,
}
