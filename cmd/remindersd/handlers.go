package main

import (
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"rokett.me/reminders/internal/models"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	reminders, err := app.reminders.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var remView []models.ReminderView
	for _, rem := range reminders {
		remView = append(remView, models.ReminderView{
			ID:                         rem.ID,
			Title:                      rem.Title,
			Description:                rem.Description,
			Frequency:                  rem.Frequency,
			FrequencyUnit:              rem.FrequencyUnit,
			StartDate:                  rem.StartDate,
			NotificationDate:           rem.NotificationDate,
			NotificationEmailAddresses: strings.Split(rem.NotificationEmailAddresses, ","),
			Recurring:                  rem.Recurring,
		})
	}

	app.render(w, r, "home.page.html", &templateData{
		Reminders:       remView,
		Flash:           app.session.PopString(r, "flash"),
		IsAuthenticated: true,
	})
}

func (app *application) showLoginPage(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "login.page.html", &templateData{
		Flash:           app.session.PopString(r, "flash"),
		IsAuthenticated: false,
	})
}

func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	conn, err := app.ldap.Connect()
	if err != nil {
		app.logger.Error(err)
		app.serverError(w, err)
		return
	}

	app.ldap.Conn = conn
	defer app.ldap.Conn.Close()

	// We replace any * characters as this is a wildcard in LDAP queries.  We only want to allow an exact match.
	u, err := app.ldap.Authenticate(strings.Replace(r.PostForm.Get("username"), "*", "", -1), r.PostForm.Get("password"), app.ldap.AllowedGroupDN)
	if err != nil {
		app.logger.Error(err)

		app.session.Put(r, "flash", "Authentication failed")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	err = app.users.Upsert(u)
	if err != nil {
		app.logger.Error(err)

		app.session.Put(r, "flash", "Authentication failed")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:   "session_token",
		Value:  u.Token,
		MaxAge: u.TokenMaxAge,
		Path:   "/",
	})

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (app *application) showReminder(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	reminder, err := app.reminders.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.Itoa(id))
	form.Set("title", reminder.Title)
	form.Set("description", reminder.Description)
	form.Set("frequency", strconv.Itoa(reminder.Frequency))
	form.Set("frequency_unit", reminder.FrequencyUnit)
	form.Set("start_date", reminder.StartDate.Format("2006-01-02"))
	form.Set("notification_date", reminder.NotificationDate.Format("2006-01-02"))
	form.Set("notifications", reminder.NotificationEmailAddresses)
	form.Set("recurring", strconv.FormatBool(reminder.Recurring))

	action := "view"
	if r.URL.Query().Get("action") == "ack" {
		action = "ack"

		if reminder.Recurring {
			switch strings.ToLower(reminder.FrequencyUnit) {
			case "day(s)":
				form.Set("new_notification_date", time.Now().AddDate(0, 0, reminder.Frequency).Format("02 Jan 2006"))
			case "month(s)":
				form.Set("new_notification_date", time.Now().AddDate(0, reminder.Frequency, 0).Format("02 Jan 2006"))
			case "year(s)":
				form.Set("new_notification_date", time.Now().AddDate(reminder.Frequency, 0, 0).Format("02 Jan 2006"))
			}
		}
	}

	app.render(w, r, "reminder_details.page.html", &templateData{
		FormData:          form,
		Action:            action,
		RecurringReminder: reminder.Recurring,
		IsAuthenticated:   true,
	})
}

func (app *application) showAddReminderForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "reminder_details.page.html", &templateData{
		Action:          "add",
		IsAuthenticated: true,
	})
}

func (app *application) addReminder(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	validationErrors := make(map[string]string)

	recurring := false
	if r.PostForm.Get("recurring") == "on" {
		recurring = true
	}

	if strings.TrimSpace(r.PostForm.Get("title")) == "" {
		validationErrors["title"] = "Cannot be blank"
	}

	if strings.TrimSpace(r.PostForm.Get("description")) == "" {
		validationErrors["description"] = "Cannot be blank"
	}

	// Crazily complex regex validation for email addresses :-) Taken from https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address.
	var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	for _, e := range strings.Split(r.PostForm.Get("notifications"), ",") {
		// In addition to the regex, email addresses also have a practical limit of 254 bytes
		if len(e) > 254 || !rxEmail.MatchString(e) {
			validationErrors["notifications"] = "Invalid email address(es)"
			break
		}
	}

	if !recurring && strings.TrimSpace(r.PostForm.Get("notification_date")) == "" {
		validationErrors["notification_date"] = "MUST be set"
	}

	if recurring && strings.TrimSpace(r.PostForm.Get("start_date")) == "" {
		validationErrors["start_date"] = "MUST be set"
	}

	if recurring && strings.TrimSpace(r.PostForm.Get("frequency")) == "" {
		validationErrors["frequency"] = "Cannot be blank"
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "reminder_details.page.html", &templateData{
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	reminder := models.Reminder{
		Title:                      r.PostForm.Get("title"),
		Description:                r.PostForm.Get("description"),
		NotificationEmailAddresses: r.PostForm.Get("notifications"),
	}

	if recurring {
		startDate, err := time.Parse("2006-01-02", r.PostForm.Get("start_date"))
		if err != nil {
			app.serverError(w, err)
			return
		}

		frequency, err := strconv.Atoi(r.PostForm.Get("frequency"))
		if err != nil {
			app.serverError(w, err)
			return
		}

		reminder.StartDate = startDate
		reminder.NotificationDate = startDate
		reminder.Recurring = recurring
		reminder.Frequency = frequency
		reminder.FrequencyUnit = r.PostForm.Get("frequency_unit")
	} else {
		notificationDate, err := time.Parse("2006-01-02", r.PostForm.Get("notification_date"))
		if err != nil {
			app.serverError(w, err)
			return
		}

		reminder.NotificationDate = notificationDate
	}

	err = app.reminders.Insert(reminder)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created reminder: "+r.PostForm.Get("title"))

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (app *application) ackReminder(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	reminder, err := app.reminders.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if reminder.Recurring {
		switch strings.ToLower(reminder.FrequencyUnit) {
		case "day(s)":
			reminder.NotificationDate = time.Now().AddDate(0, 0, reminder.Frequency)
		case "month(s)":
			reminder.NotificationDate = time.Now().AddDate(0, reminder.Frequency, 0)
		case "year(s)":
			reminder.NotificationDate = time.Now().AddDate(reminder.Frequency, 0, 0)
		}

		err = app.reminders.Update(reminder)
		if err != nil {
			app.serverError(w, err)
			return
		}
	} else {
		err = app.reminders.Delete(id)
		if err != nil {
			app.serverError(w, err)
			return
		}
	}

	app.session.Put(r, "flash", "Acknowledged reminder: "+reminder.Title)

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (app *application) updateReminder(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	reminder, err := app.reminders.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	validationErrors := make(map[string]string)

	if !reminder.Recurring && r.PostForm.Get("recurring") == "on" {
		reminder.Recurring = true
	}
	if reminder.Recurring && r.PostForm.Get("recurring") != "on" {
		reminder.Recurring = false
		reminder.Frequency = 0
		reminder.FrequencyUnit = ""
		reminder.StartDate = time.Time{}
	}

	if strings.TrimSpace(r.PostForm.Get("title")) == "" {
		validationErrors["title"] = "Cannot be blank"
	}

	if strings.TrimSpace(r.PostForm.Get("description")) == "" {
		validationErrors["description"] = "Cannot be blank"
	}

	// Crazily complex regex validation for email addresses :-) Taken from https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address.
	var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	for _, e := range strings.Split(r.PostForm.Get("notifications"), ",") {
		// In addition to the regex, email addresses also have a practical limit of 254 bytes
		if len(e) > 254 || !rxEmail.MatchString(e) {
			validationErrors["notifications"] = "Invalid email address(es)"
			break
		}
	}

	if !reminder.Recurring && strings.TrimSpace(r.PostForm.Get("notification_date")) == "" {
		validationErrors["notification_date"] = "MUST be set"
	}

	if reminder.Recurring && strings.TrimSpace(r.PostForm.Get("start_date")) == "" {
		validationErrors["start_date"] = "MUST be set"
	}

	if reminder.Recurring && strings.TrimSpace(r.PostForm.Get("frequency")) == "" {
		validationErrors["frequency"] = "Cannot be blank"
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "reminder_details.page.html", &templateData{
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	reminder.Title = r.PostForm.Get("title")
	reminder.Description = r.PostForm.Get("description")
	reminder.NotificationEmailAddresses = r.PostForm.Get("notifications")

	if reminder.Recurring {
		startDate, err := time.Parse("2006-01-02", r.PostForm.Get("start_date"))
		if err != nil {
			app.serverError(w, err)
			return
		}

		frequency, err := strconv.Atoi(r.PostForm.Get("frequency"))
		if err != nil {
			app.serverError(w, err)
			return
		}

		reminder.StartDate = startDate
		reminder.NotificationDate = startDate
		reminder.Frequency = frequency
		reminder.FrequencyUnit = r.PostForm.Get("frequency_unit")
	} else {
		notificationDate, err := time.Parse("2006-01-02", r.PostForm.Get("notification_date"))
		if err != nil {
			app.serverError(w, err)
			return
		}

		reminder.NotificationDate = notificationDate
	}

	err = app.reminders.Update(reminder)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated reminder: "+r.PostForm.Get("title"))

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (app *application) deleteReminder(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	reminder, err := app.reminders.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.reminders.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted reminder: "+reminder.Title)

	http.Redirect(w, r, "/", http.StatusSeeOther)
}
