package main

import (
	"bytes"
	"fmt"
	"net/mail"
	"net/smtp"
	"strings"
	"text/template"
	"time"

	"rokett.me/reminders"
	"rokett.me/reminders/internal/models"
)

type EmailMsg struct {
	Title string
	Text  []string
}

func (app *application) sendReminder(reminder models.Reminder, due string) {
	smtpAddr := fmt.Sprintf("%s:%d", app.cfg.Email.Host, app.cfg.Email.Port)

	from := mail.Address{
		Name:    app.cfg.Email.From,
		Address: app.cfg.Email.From,
	}

	var emailHeaderTo []string
	var rcpts []mail.Address

	for _, v := range strings.Split(reminder.NotificationEmailAddresses, ",") {
		to := mail.Address{
			Name:    "",
			Address: v,
		}

		emailHeaderTo = append(emailHeaderTo, to.String())
		rcpts = append(rcpts, to)
	}

	// Setup headers
	headers := map[string]string{
		"From":    from.String(),
		"To":      strings.Join(emailHeaderTo, ","),
		"Subject": fmt.Sprintf("Reminder - %s", reminder.Title),
	}

	t, err := template.ParseFS(reminders.EmailTemplates, "email_templates/reminder.html")
	if err != nil {
		app.logger.Errorw("unable to parse email template",
			"error", err,
		)
		return
	}

	msgText := []string{
		fmt.Sprintf("This reminder is %s.", due),
		"&nbsp;",
		fmt.Sprint(reminder.Description),
		"&nbsp;",
		fmt.Sprintf("Go to %s/reminders/%d?action=ack to view and reset the reminder.", app.cfg.ApplicationUrl, reminder.ID),
	}

	var emailBody bytes.Buffer

	err = t.Execute(&emailBody, EmailMsg{
		Title: reminder.Title,
		Text:  msgText,
	})
	if err != nil {
		app.logger.Errorw("problem executing email template",
			"error", err,
		)
		return
	}

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	message += "\r\n" + emailBody.String()

	maxAttempts := 10

	var c *smtp.Client
	var sErr error
	for attempt := 1; attempt <= maxAttempts; attempt++ {
		c, sErr = smtp.Dial(smtpAddr)
		if sErr != nil {
			app.logger.Errorw("unable to dial SMTP server; retrying",
				"attempt", attempt,
				"error", sErr,
			)

			if attempt == 10 {
				break
			}

			time.Sleep(time.Duration(attempt) * (time.Duration(attempt) * time.Second))
			continue
		}

		break
	}
	if sErr != nil {
		app.logger.Errorw("timed out connecting to SMTP server",
			"error", sErr,
			"max_attempts", maxAttempts,
		)
		return
	}

	for _, to := range rcpts {
		if err = c.Mail(from.Address); err != nil {
			app.logger.Errorw("unable to set FROM address",
				"error", err,
			)
			continue
		}

		if err = c.Rcpt(to.Address); err != nil {
			app.logger.Errorw("unable to set TO address",
				"error", err,
			)
			continue
		}

		w, err := c.Data()
		if err != nil {
			app.logger.Error("problem sending email",
				"error", err,
			)
			continue
		}

		_, err = w.Write([]byte(message))
		if err != nil {
			app.logger.Error("problem writing email",
				"error", err,
			)
			continue
		}

		err = w.Close()
		if err != nil {
			app.logger.Error("unable to close client data",
				"error", err,
			)
		}

		app.logger.Debugw("sent reminder",
			"to", to.Address,
			"reminder", reminder.Title,
		)
	}

	err = c.Quit()
	if err != nil {
		app.logger.Error("unable to quit SMTP client",
			"error", err,
		)
		return
	}
}
