package main

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func setupLogger(app string, version string, build string) (*zap.Logger, *zap.SugaredLogger) {
	w := zapcore.AddSync(os.Stdout)

	// Need to create a new encoder config in order to print the timestamp in RFC339 format
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoder := zapcore.NewJSONEncoder(encoderConfig)

	// Now we can create a new Zap core using the custom encoder, the custom writer, and setting the default level
	level := zap.InfoLevel

	if os.Getenv("reminders_debug") != "" {
		level = zap.DebugLevel
	}

	zapCore := zapcore.NewCore(encoder, w, level)

	// And finally create the logger, including the caller details
	zapLogger := zap.New(zapCore, zap.AddCaller(), zap.AddStacktrace(zapcore.ErrorLevel))

	zapLogger = zapLogger.With(
		zap.String("app", app),
		zap.String("version", version),
		zap.String("build", build),
	)

	return zapLogger, zapLogger.Sugar()
}
