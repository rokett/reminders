package main

import (
	"time"

	"github.com/robfig/cron/v3"
)

func (app *application) runCron() {
	c := cron.New()

	_, err := c.AddFunc(app.cfg.CronInterval, func() {
		reminders, err := app.reminders.List()
		if err != nil {
			app.logger.Errorw("unable to retrieve reminders to send notifications",
				"error", err,
			)
			return
		}

		year, month, day := time.Now().Date()
		today := time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

		for _, r := range reminders {
			if r.NotificationDate.Before(today) {
				app.sendReminder(r, "overdue")
				continue
			}

			if r.NotificationDate.Equal(today) {
				app.sendReminder(r, "due today")
				continue
			}

			due := r.NotificationDate.Sub(today).Hours()

			switch {
			case due < 168:
				app.sendReminder(r, "due in less than 7 days")
				continue
			case due == 336:
				app.sendReminder(r, "due in 14 days")
				continue
			case due == 720:
				app.sendReminder(r, "due in 30 days")
				continue
			}
		}
	})
	if err != nil {
		app.logger.Errorw("problem adding scheduled function to send reminder notifications",
			"error", err,
		)
	}

	c.Start()
}
