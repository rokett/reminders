package main

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/microsoft/go-mssqldb"
	"go.uber.org/zap"
)

func openDB(dbHost string, dbPort int, dbName string, dbUsername string, dbPassword string, logger *zap.SugaredLogger) (db *sqlx.DB) {
	var dbError error

	maxAttempts := 10

	dsn := fmt.Sprintf("server=%s;port=%d;database=%s;encrypt=disable;connection timeout=30", dbHost, dbPort, dbName)

	if dbUsername != "" && dbPassword != "" {
		dsn = fmt.Sprintf("%s;user id=%s;password=%s", dsn, dbUsername, dbPassword)
	}

	for attempt := 1; attempt <= maxAttempts; attempt++ {
		db, dbError = sqlx.Connect("sqlserver", dsn)
		if dbError != nil {
			logger.Infow("unable to connect to database; retrying.",
				"attempt", attempt,
				"error", dbError,
			)

			time.Sleep(time.Duration(attempt) * time.Second)
			continue
		}

		break
	}

	if dbError != nil {
		logger.Fatalw("run out of attempts to connect to database",
			"error", dbError,
			"max_attempts", maxAttempts,
		)
	}

	return db
}
