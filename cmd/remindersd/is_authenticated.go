package main

import (
	"errors"
	"fmt"
	"net/http"
)

func (app *application) isAuthenticated(r *http.Request) error {
	token, err := r.Cookie("session_token")
	if err != nil {
		return fmt.Errorf("unable to retrieve session token: %w", err)
	}

	valid, user, err := app.users.IsTokenValid(token.Value)
	if err != nil {
		return err
	}
	if !valid {
		return errors.New("session token is not valid")
	}

	enabled, u, err := app.ldap.IsUserEnabled(user.Username)
	if err != nil {
		return err
	}
	if !enabled {
		return errors.New("user account is not enabled in ldap directory")
	}

	err = app.ldap.IsInAllowedGroup(app.ldap.AllowedGroupDN, u.GetAttributeValues("memberOf"))
	if err != nil {
		return errors.New("access denied")
	}

	return nil
}
