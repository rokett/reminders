package main

import (
	"io/fs"
	"net/http"

	"rokett.me/reminders"

	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (app *application) routes() http.Handler {
	r := chi.NewRouter()

	// status route is used for healthchecking and is excluded from logging
	r.Get("/status", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := w.Write([]byte("ok"))
		if err != nil {
			app.serverError(w, err)
		}
	})

	// metrics route is excluded from logging
	r.Method(http.MethodGet, "/metrics", promhttp.Handler())

	r.Group(func(r chi.Router) {
		r.Use(app.logMetrics)
		r.Use(app.recoverPanic)
		r.Use(app.getClientIP)
		r.Use(app.logRequest)
		r.Use(app.secureHeaders)
		r.Use(app.cacheControl)
		r.Use(app.session.Enable)

		r.Get("/login", app.showLoginPage)
		r.Post("/login", app.loginUser)
	})

	r.Group(func(r chi.Router) {
		r.Use(app.logMetrics)
		r.Use(app.recoverPanic)
		r.Use(app.getClientIP)
		r.Use(app.logRequest)
		r.Use(app.secureHeaders)
		r.Use(app.cacheControl)
		r.Use(app.requireAuthentication)
		r.Use(app.session.Enable)

		r.Get("/", app.home)

		r.Get("/reminders/add", app.showAddReminderForm)
		r.Get("/reminders/{id}", app.showReminder)
		r.Post("/reminders/add", app.addReminder)
		r.Post("/reminders/{id}/ack", app.ackReminder)
		r.Put("/reminders/{id}", app.updateReminder)
		r.Delete("/reminders/{id}", app.deleteReminder)
	})

	var static, err = fs.Sub(reminders.UI, "ui/dist/static")
	if err != nil {
		app.logger.Fatalw("unable to return embedded assets",
			"error", err,
		)
	}

	r.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.FS(static))))

	return r
}
