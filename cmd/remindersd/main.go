package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"go.uber.org/zap"

	"github.com/golangcollege/sessions"
	"rokett.me/reminders/internal/ldap"
	"rokett.me/reminders/internal/models"
)

type contextKeyType string

const clientIPCtxKey contextKeyType = "client_ip"

var (
	app     = "Reminders"
	version string
	build   string
)

func main() {
	cfg := new(Config)
	flag.IntVar(&cfg.Port, "port", 4000, "Port to listen on")
	flag.StringVar(&cfg.Database.Host, "db_host", "127.0.0.1", "Name of database host, including instance name if needed")
	flag.IntVar(&cfg.Database.Port, "db_port", 1433, "Port to connect to database server on")
	flag.StringVar(&cfg.Database.Name, "db", "reminders", "Name of the database to use")
	flag.StringVar(&cfg.Database.Username, "db_username", "", "Username of account with read/write access to database")
	flag.StringVar(&cfg.Database.Password, "db_password", "", "Password for database user")
	flag.StringVar(&cfg.Ldap.DirectoryServers, "ldap_directory_servers", "", "Comma separated string of LDAP directory servers")
	flag.IntVar(&cfg.Ldap.Port, "ldap_port", 389, "Defaults to 389, or 636 if SSL is used")
	flag.StringVar(&cfg.Ldap.BaseDN, "ldap_base_dn", "", "Base LDAP DN to search for objects")
	flag.StringVar(&cfg.Ldap.BindDN, "ldap_bind_dn", "", "Distinguished name of read only user account to use to query the LDAP directory")
	flag.StringVar(&cfg.Ldap.BindPW, "ldap_bind_pw", "", "Password for the read only user used to query the LDAP directory")
	flag.StringVar(&cfg.Ldap.AllowedGroupDN, "allowed_group_dn", "", "Distinguished name of the group which gives users access to the application; nested groups are allowed")
	flag.StringVar(&cfg.Email.Host, "email_host", "", "SMTP host")
	flag.IntVar(&cfg.Email.Port, "email_port", 25, "SMTP port")
	flag.StringVar(&cfg.Email.From, "email_from_address", "", "From address for email sent from Heimdall")
	flag.StringVar(&cfg.ApplicationUrl, "application_url", "http://localhost:4000", "URL that the application can be found on; used in links that are emailed in reminders")
	flag.StringVar(&cfg.CronInterval, "cron_interval", "0 3 * * *", "Interval that reminders should be processed on")

	session_secret := flag.String("session_secret", "", "Secret key for session cookies; 32 bytes long")
	versionFlg := flag.Bool("version", false, "Display application version")
	helpFlg := flag.Bool("help", false, "Display application help")

	flag.Parse()

	if *versionFlg {
		fmt.Printf("%s v%s build %s\n", app, version, build)
		os.Exit(0)
	}

	if *helpFlg {
		flag.PrintDefaults()
		os.Exit(0)
	}

	if cfg.Email.From == "" {
		fmt.Println("You MUST specify a FROM email address")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if cfg.Ldap.BaseDN == "" || cfg.Ldap.BindDN == "" || cfg.Ldap.BindPW == "" {
		fmt.Println("You MUST specify LDAP details")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if cfg.Ldap.AllowedGroupDN == "" {
		fmt.Println("You MUST specify the group DN which allows access")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *session_secret == "" {
		fmt.Println("You MUST specify a secret key for session cookies")
		flag.PrintDefaults()
		os.Exit(1)
	}

	zapLogger, logger := setupLogger(app, version, build)
	defer zapLogger.Sync()
	defer logger.Sync()

	db := openDB(cfg.Database.Host, cfg.Database.Port, cfg.Database.Name, cfg.Database.Username, cfg.Database.Password, logger)
	defer db.Close()

	err := runMigrations(logger, db)
	if err != nil {
		os.Exit(1)
	}

	templateCache, err := newTemplateCache()
	if err != nil {
		logger.Fatal(err)
	}

	session := sessions.New([]byte(*session_secret))
	session.Lifetime = 12 * time.Hour

	app := &application{
		cfg:    cfg,
		logger: logger,
		ldap: &ldap.Config{
			DirectoryServers: cfg.Ldap.DirectoryServers,
			Port:             cfg.Ldap.Port,
			BaseDN:           cfg.Ldap.BaseDN,
			BindDN:           cfg.Ldap.BindDN,
			BindPW:           cfg.Ldap.BindPW,
			AllowedGroupDN:   cfg.Ldap.AllowedGroupDN,
		},
		templateCache: templateCache,
		reminders: &models.ReminderModel{
			DB: db,
		},
		users: &models.UserModel{
			DB: db,
		},
		session: session,
	}

	// Test that LDAP config is correct
	conn, err := app.ldap.Connect()
	if err != nil {
		logger.Fatal(err)
	}
	conn.Close()

	app.runCron()

	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", app.cfg.Port),
		ErrorLog:     zap.NewStdLog(zapLogger),
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	app.logger.Infow("running server",
		"port", app.cfg.Port,
	)
	err = srv.ListenAndServe()
	app.logger.Fatal(err.Error())
}
