package main

import (
	"html/template"

	"github.com/golangcollege/sessions"
	"go.uber.org/zap"
	"rokett.me/reminders/internal/ldap"
	"rokett.me/reminders/internal/models"
)

type Config struct {
	Port           int
	Database       database
	Ldap           ldapCfg
	Email          email
	ApplicationUrl string
	CronInterval   string
}

type database struct {
	Host     string
	Port     int
	Name     string
	Username string
	Password string
}

type application struct {
	cfg           *Config
	logger        *zap.SugaredLogger
	ldap          *ldap.Config
	templateCache map[string]*template.Template
	reminders     *models.ReminderModel
	users         *models.UserModel
	session       *sessions.Session
}

type ldapCfg struct {
	DirectoryServers string
	Port             int
	BaseDN           string
	BindDN           string
	BindPW           string
	AllowedGroupDN   string
}

type email struct {
	Host string
	Port int
	From string
}
