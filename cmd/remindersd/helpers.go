package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
)

type malformedRequest struct {
	//status int
	msg string
}

func (mr *malformedRequest) Error() string {
	return mr.msg
}

func (app *application) serverError(w http.ResponseWriter, err error) {
	app.logger.Error(err.Error())

	httpErrorsTotal.WithLabelValues("5xx").Inc()

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func (app *application) clientError(w http.ResponseWriter, status int) {
	// We don't really care about specific status codes, just the type
	code := strconv.Itoa(status)
	switch code[0:1] {
	case "2":
		code = "2xx"
	case "3":
		code = "3xx"
	case "4":
		code = "4xx"
	case "5":
		code = "5xx"
	}

	httpErrorsTotal.WithLabelValues(code).Inc()

	http.Error(w, http.StatusText(status), status)
}

func (app *application) notFound(w http.ResponseWriter) {
	app.clientError(w, http.StatusNotFound)
}

func (app *application) render(w http.ResponseWriter, r *http.Request, name string, td *templateData) {
	ts, ok := app.templateCache[name]
	if !ok {
		app.serverError(w, fmt.Errorf("the %s template does not exist", name))
		return
	}

	buf := new(bytes.Buffer)

	err := ts.Execute(buf, td)
	if err != nil {
		app.serverError(w, err)
		return
	}

	_, err = buf.WriteTo(w)
	if err != nil {
		app.serverError(w, err)
		return
	}
}

func decodeJSON(b []byte, dst interface{}) error {
	/*if r.Header.Get("Content-Type") != "" {
		value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
		if value != "application/json" {
			msg := "Content-Type header is not application/json"
			return &malformedRequest{status: http.StatusUnsupportedMediaType, msg: msg}
		}
	}*/

	// Use http.MaxBytesReader to enforce a maximum read of 1MB from the
	// response body. A request body larger than that will now result in
	// Decode() returning a "http: request body too large" error.
	//r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	// Setup the decoder and call the DisallowUnknownFields() method on it.
	// This will cause Decode() to return a "json: unknown field ..." error if it encounters any extra unexpected fields in the JSON.
	// Strictly speaking, it returns an error for "keys which do not match any non-ignored, exported fields in the destination.
	dec := json.NewDecoder(bytes.NewReader(b))
	dec.DisallowUnknownFields()

	err := dec.Decode(&dst)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
		// Catch any syntax errors in the JSON and send an error message
		// which interpolates the location of the problem to make it
		// easier for the client to fix.
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			//return &malformedRequest{status: http.StatusBadRequest, msg: msg}
			return &malformedRequest{msg: msg}

		// In some circumstances Decode() may also return an
		// io.ErrUnexpectedEOF error for syntax errors in the JSON.
		// There is an open issue regarding this at https://github.com/golang/go/issues/25956.
		case errors.Is(err, io.ErrUnexpectedEOF):
			msg := fmt.Sprintf("Request body contains badly-formed JSON")
			//return &malformedRequest{status: http.StatusBadRequest, msg: msg}
			return &malformedRequest{msg: msg}

		// Catch any type errors, like trying to assign a string in the
		// JSON request body to a int field in our Person struct.
		// We can interpolate the relevant field name and position into the error
		// message to make it easier for the client to fix.
		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
			//return &malformedRequest{status: http.StatusBadRequest, msg: msg}
			return &malformedRequest{msg: msg}

		// Catch the error caused by extra unexpected fields in the request body.
		// We extract the field name from the error message and interpolate it in our custom error message.
		// There is an open issue at https://github.com/golang/go/issues/29035 regarding turning this into a sentinel error.
		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
			//return &malformedRequest{status: http.StatusBadRequest, msg: msg}
			return &malformedRequest{msg: msg}

		// An io.EOF error is returned by Decode() if the request body is empty.
		case errors.Is(err, io.EOF):
			msg := "Request body must not be empty"
			//return &malformedRequest{status: http.StatusBadRequest, msg: msg}
			return &malformedRequest{msg: msg}

		// Catch the error caused by the request body being too large. Again
		// there is an open issue regarding turning this into a sentinel
		// error at https://github.com/golang/go/issues/30715.
		case err.Error() == "http: request body too large":
			msg := "Request body must not be larger than 1MB"
			//return &malformedRequest{status: http.StatusRequestEntityTooLarge, msg: msg}
			return &malformedRequest{msg: msg}

		// Otherwise default to logging the error.
		default:
			return err
		}
	}

	// Call decode again, using a pointer to an empty anonymous struct as the destination.
	// If the body only contained a single JSON object this will return an io.EOF error.
	// So if we get anything else, we know that there is additional data in the body.
	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		msg := "Request body must only contain a single JSON object"
		//return &malformedRequest{status: http.StatusBadRequest, msg: msg}
		return &malformedRequest{msg: msg}
	}

	return nil
}
