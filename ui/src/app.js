if (document.getElementById("recurring")) {
    if (document.getElementById("recurring").checked) {
        document.getElementById("recurringInputs").classList.toggle("hidden")
        toggleDisabledInput(document.getElementById("notification_date"))
    }

    // Toggle recurring reminder form elements
    document.getElementById("recurring").addEventListener("click", function(){
        document.getElementById("recurringInputs").classList.toggle("hidden")
        toggleDisabledInput(document.getElementById("notification_date"))
    })
}

// Fade and remove flash message
if (document.getElementById("flash")) {
    if (document.getElementById("flash").parentElement.id !== "login") {
        fadeFlash()
    }
}

async function fadeFlash() {
    await wait(2000)
    document.getElementById("flash").classList.add("transition-opacity", "duration-1000", "ease-out", "opacity-0")
    await wait(950)
    document.getElementById("flash").classList.add("hidden")
}

// Helper functions
async function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

function toggleDisabledInput(el) {
    if (el.disabled) {
        el.disabled = false
    } else {
        el.disabled = true
    }
}
