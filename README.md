# Reminders

Nothing more than a pretty simple application to add reminders to.  One-off and recurring reminders can be added, and notifications will be sent to the email addresses specified for the reminder notifications on the following schedule:

- 30 days prior to due date.
- 14 days prior to due date.
- Every day when reminder is due in less than 7 days.
- Every day when reminder is overdue.

Notifications are processed once per day, by default at 03:00 although the time can be overriden by a flag.

The notification includes a link to acknowledge, or complete, the reminder.  If a one-off reminder is acknowledged it will be deleted. If a recurring reminder is acknowledged the due date will be reset based on the recurring frequency and the date the reminder is acknowledged.

For example, a reminder with a due date of 20/01/2022 and a recurring frequency of every 3 months, which is acknowledged on 10/01/2022 will have its due date reset to 3 months after 10/01/2022 which is 10/04/2022.

## Configuring the server

The following flags are available for configuring the server.

| Flag                     | Description                                                                                            | Default Value         | Required?                              |
| ------------------------ | ------------------------------------------------------------------------------------------------------ | --------------------- | -------------------------------------- |
| `port`                   | The port that the server will listen on for web requests                                               | 4000                  | No                                     |
| `db_host`                | The IP address of the database host, including instance name if needed                                 | 127.0.0.1             | No                                     |
| `db_port`                | Port to connect to database server on                                                                  | 1433                  | No                                     |
| `db`                     | Name of the database to use                                                                            | reminders             | No                                     |
| `db_username`            | Username of account with read/write access to database                                                 |                       | Yes - If not using integrated security |
| `db_password`            | Password for database user                                                                             |                       | Yes - If not using integrated security |
| `email_host`             | The SMTP host used to send email if the email handler is used                                          |                       | No                                     |
| `email_port`             | The TCP port to use to access the SMTP host                                                            | 25                    | No                                     |
| `email_from_address`     | From address for email sent from                                                                       |                       | No                                     |
| `ldap_directory_servers` | Comma separated string of LDAP directory servers to authenticate against                               |                       | Yes                                    |
| `ldap_port`              | Port that directory servers are listening on                                                           | 389                   | No                                     |
| `ldap_base_dn`           | Base LDAP DN to search for objects                                                                     |                       | Yes                                    |
| `ldap_bind_dn`           | Distinguished name of read only user account to use to query the LDAP directory                        |                       | Yes                                    |
| `ldap_bind_pw`           | Password for the read only user used to query the LDAP directory                                       |                       | Yes                                    |
| `allowed_group_dn`       | Distinguished name of the group which gives users access to the application; nested groups are allowed |                       | Yes                                    |
| `application_url`        | URL that the application can be found onl used in links that are emailed in notifications              | http://localhost:4000 | No                                     |
| `cron_interval`          | Interval that reminder notifications should be processed on                                            | 0 3 * * *             | No                                     |
| `session_secret`         | Secret key for session cookies; max of 32 bytes long                                                   |                       | Yes                                    |
| `version`                | Display the version number only                                                                        | false                 | No                                     |
| `help`                   | Display application help                                                                               | false                 | No                                     |

To enable debug logging, set an environment variable called `reminders_debug` to anything you want and restart the server; the value is not important, just that the environment variable exists.

## How to setup local development environment
1. Install NodeJS (v14.15.0 or higher)
2. Install SQL Server Express.
3. Configure services to startup automatically.
4. SQL Server Configuration Manager -> SQL Server Network Configuration -> Protocols for SQLEXPRESS -> Enable TCP/IP
5. Create database in SQL
6. Apply `db_datareader`, `db_datawriter`, `db_ddladmin` permissions to the user you'll be running the application as.
7. Run `remindersd.exe` with the required flags in order to run database migrations and start the application.
