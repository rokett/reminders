export CGO_ENABLED = 0
export GOOS = linux
export GOARCH = amd64

VERSION := 1.6.0
BUILD := $(shell git rev-list -1 HEAD)
LDFLAGS := -ldflags "-X main.version=$(VERSION) -X main.build=$(BUILD) -s -w -extldflags '-static'"

build:
	chmod 755 ./node_modules/.bin/tailwindcss
	chmod 755 ./node_modules/.bin/cleancss
	npx tailwindcss -i ./ui/src/app.css -o ./ui/tmp/app.css
	npx cleancss -o ./ui/tmp/app.min.css ./ui/tmp/app.css
	npm run build
	go build -a -mod=vendor $(LDFLAGS) -o remindersd ./cmd/remindersd

docker:
	docker build --no-cache -t rokett/reminders:v$(VERSION) .
	docker push rokett/reminders:v$(VERSION)

publish: docker
	docker build --no-cache -t rokett/reminders:latest .
	docker push rokett/reminders:latest

outdated:
	npm outdated
	go list -u -m -mod=mod -json all | go-mod-outdated -update -direct
