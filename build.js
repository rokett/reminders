const fs = require('fs')
const fse = require('fs-extra')
const replace = require('replace-in-file')
const crypto = require('crypto')
const {minify} = require("terser")

async function build() {
    // Initialise dist folder
    try {
        if (fs.existsSync('ui/dist')) {
            fs.rmSync('ui/dist', {
                recursive: true,
                force: true
            })
        }

        fs.mkdirSync('ui/dist')
        fs.mkdirSync('ui/dist/static')
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    // Copy source files to dist
    try {
        fse.copySync('ui/src/html', 'ui/dist/html')
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    var jsPaths = [
        'node_modules/htmx.org/dist/htmx.min.js',
    ]

    var js = ""

    jsPaths.forEach(file => {
        js += fs.readFileSync(file, {
            encoding: 'utf8'
        })
    })

    var appjs = fs.readFileSync('ui/src/app.js', {
        encoding: 'utf8'
    })

    var result = await minify(appjs, {})
    js += result.code

    // Bundle JS to a single file
    try {
        fs.writeFileSync('ui/tmp/app.js', js)
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    // Bundle CSS to a single file and compress
    var cssPaths = [
        'ui/tmp/app.min.css',
        'node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css',
        'node_modules/@fortawesome/fontawesome-free/css/solid.min.css'
    ]

    var css = ""

    cssPaths.forEach(file => {
        css += fs.readFileSync(file, {
            encoding: 'utf8'
        })
    })

    try {
        fs.writeFileSync('ui/tmp/app.css', css)
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    var staticFonts =[
        'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.ttf',
        'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2'
    ]

    staticFonts.forEach(file => {
        let start = file.lastIndexOf('/') + 1
        let origFilename = file.substring(start)

        let parts = origFilename.split('.')

        let fileContents = fs.readFileSync(file, {})

        newFilename = parts[0] + '_' + crypto.createHash('md5').update(fileContents).digest('hex') + '.' + parts[1]

        fs.copyFileSync(file, 'ui/dist/static/' + newFilename)

        let re = new RegExp('../webfonts/' + origFilename, 'g')
        replace.sync({
            files: 'ui/tmp/app.css',
            from: re,
            to: newFilename,
        })
    })

    var staticFiles =[
        'ui/tmp/app.js',
        'ui/tmp/app.css',
        'ui/src/logo.svg'
    ]

    staticFiles.forEach(file => {
        let start = file.lastIndexOf('/') + 1
        let origFilename = file.substring(start)

        let parts = origFilename.split('.')

        let fileContents = fs.readFileSync(file, {})

        newFilename = parts[0] + '_' + crypto.createHash('md5').update(fileContents).digest('hex') + '.' + parts[1]

        fs.copyFileSync(file, 'ui/dist/static/' + newFilename)

        let re = new RegExp(origFilename, 'g')
        replace.sync({
            files: 'ui/dist/html/base.layout.html',
            from: re,
            to: newFilename,
        })
    })
}

build()
