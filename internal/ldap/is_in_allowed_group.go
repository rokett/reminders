package ldap

func (l *Config) IsInAllowedGroup(allowedGroupDN string, groups []string) error {
	for _, g := range groups {
		if g == allowedGroupDN {
			return nil
		}
	}

	// groupDN will contain the DN of each group, including nested groups, that the user is a member of.
	// groupDN is the variable returned to the calling function.
	groupDN := make(map[string]bool)

	for {
		// Temporary slice to contain DNs of groups which are the parent(s) for the specific group being searched in each loop.
		var nextLevelGroups []string

		for _, g := range groups {
			if g == allowedGroupDN {
				return nil
			}

			// Need to ensure we don't create duplicates.
			// If the DN of the group for this iteration matches one already in the groupDN map
			// we need to skip this loop and move onto the next one.  Without this check we'd end up in an endless loop.
			if groupDN[g] {
				continue
			}

			groupDN[g] = true

			// We need to see if the group is a member of any other groups (parents).
			parentGroups, err := l.getGroupMembers(g)
			if err != nil {
				return err
			}

			// If there are any parent groups we assign them to a temporary slice for processing after the
			// current slice of groups has been checked.
			if len(parentGroups) > 0 {
				nextLevelGroups = append(nextLevelGroups, parentGroups...)
			}
		}

		// If there are no more parent groups to check then we must have reached the top of the tree.
		// If so we can exit the loop and return data to the calling function.
		if len(nextLevelGroups) == 0 {
			break
		}

		// If we're still here then there are still groups to search.
		// Start the loop again with the next set of groups in the tree.
		groups = nextLevelGroups
	}

	return ErrAccessDenied
}
