package ldap

import (
	"fmt"
	"regexp"

	"github.com/go-ldap/ldap/v3"
)

func (l *Config) IsUserEnabled(username string) (bool, *ldap.Entry, error) {
	searchRequest := ldap.NewSearchRequest(
		l.BaseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Person)(userPrincipalName=%s))", username),
		[]string{"memberOf", "userPrincipalName"},
		nil,
	)

	sr, err := l.Conn.Search(searchRequest)
	if err != nil {
		return false, nil, fmt.Errorf("error searching for user object, %s,  in LDAP directory: %w", username, err)
	}

	if len(sr.Entries) < 1 {
		return false, nil, ErrNoUserInLdapDirectory
	}

	if len(sr.Entries) > 1 {
		return false, nil, ErrMultipleUsersInLdapDirectory
	}

	// 514 = Disabled account
	// 546 = Disabled, password not required
	// 66050 = Disabled, Password Doesn’t Expire
	// 66082 = Disabled, Password Doesn’t Expire & Not Required
	// 262658 = Disabled, Smartcard Required
	// 262690 = Disabled, Smartcard Required, Password Not Required
	// 328194 = Disabled, Smartcard Required, Password Doesn’t Expire
	// 328226 = Disabled, Smartcard Required, Password Doesn’t Expire & Not Required
	re := regexp.MustCompile(`(?m)^(?:514|546|66050|66082|262658|262690|328194|328226)$`)
	if len(re.FindStringIndex(sr.Entries[0].GetAttributeValue("userAccountControl"))) > 0 {
		return false, nil, nil
	}

	return true, sr.Entries[0], nil
}
