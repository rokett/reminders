package ldap

import "github.com/go-ldap/ldap/v3"

type Config struct {
	DirectoryServers string
	Port             int
	BaseDN           string
	BindDN           string
	BindPW           string
	Conn             *ldap.Conn
	AllowedGroupDN   string
}
