package ldap

import (
	"fmt"
	"strings"

	"github.com/go-ldap/ldap/v3"
)

func (l *Config) Connect() (*ldap.Conn, error) {
	var hosts []string
	tmp := strings.Split(l.DirectoryServers, ",")
	for _, v := range tmp {
		hosts = append(hosts, strings.TrimSpace(v))
	}

	var err error
	var conn *ldap.Conn

	for _, ds := range hosts {
		conn, err = ldap.DialURL(fmt.Sprintf("ldap://%s:%d", ds, l.Port))
		if err != nil {
			continue
		}

		err := conn.Bind(l.BindDN, l.BindPW)
		if err != nil {
			return nil, fmt.Errorf("error during LDAP bind: %w", err)
		}

		return conn, nil
	}

	return nil, fmt.Errorf("error connecting to LDAP directory server: %w", err)
}
