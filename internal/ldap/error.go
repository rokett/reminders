package ldap

import "errors"

var (
	ErrNoUserInLdapDirectory        = errors.New("user does not exist in LDAP directory")
	ErrMultipleUsersInLdapDirectory = errors.New("multiple users returned from LDAP directory")
	ErrLdapUserObjectDisabled       = errors.New("user account is disabled in LDAP directory")
	ErrNotFound                     = errors.New("no objects found")
	ErrAccessDenied                 = errors.New("access denied")
	ErrAccountDisabled              = errors.New("user account is not enabled in ldap directory")
)
