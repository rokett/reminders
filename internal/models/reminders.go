package models

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

var ErrNoRecords = errors.New("no matching reminders found")

type Reminder struct {
	ID                         int       `db:"id"`
	Title                      string    `db:"title"`
	Description                string    `db:"description"`
	Frequency                  int       `db:"frequency"`
	FrequencyUnit              string    `db:"frequency_unit"`
	StartDate                  time.Time `db:"start_date"`
	NotificationDate           time.Time `db:"notification_date"`
	NotificationEmailAddresses string    `db:"notification_email_addresses"`
	Recurring                  bool      `db:"recurring"`
	CreatedAt                  time.Time `db:"created_at"`
	UpdatedAt                  time.Time `db:"updated_at"`
}

type ReminderView struct {
	ID                         int
	Title                      string
	Description                string
	Frequency                  int
	FrequencyUnit              string
	StartDate                  time.Time
	NotificationDate           time.Time
	NotificationEmailAddresses []string
	Recurring                  bool
}

type ReminderModel struct {
	DB       *sqlx.DB
	Reminder Reminder
}

func (r *ReminderModel) List() ([]Reminder, error) {
	var reminders []Reminder

	err := r.DB.Select(&reminders, "SELECT id, title, description, frequency, frequency_unit, start_date, notification_date, notification_email_addresses, recurring, created_at, updated_at FROM reminders ORDER BY title;")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return reminders, ErrNoRecords
	}
	if err != nil {
		return reminders, fmt.Errorf("unable to get remidners: %w", err)
	}

	return reminders, nil
}

func (r *ReminderModel) Get(id int) (Reminder, error) {
	var reminder Reminder

	err := r.DB.Get(&reminder, "SELECT id, title, description, frequency, frequency_unit, start_date, notification_date, notification_email_addresses, recurring FROM reminders WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return reminder, ErrNoRecords
	}
	if err != nil {
		return reminder, fmt.Errorf("unable to get reminder with id %d: %w", id, err)
	}

	return reminder, nil
}

func (r *ReminderModel) Insert(reminder Reminder) error {
	_, err := r.DB.NamedExec("INSERT INTO reminders (title, description, frequency, frequency_unit, start_date, notification_date, notification_email_addresses, recurring) VALUES (:title, :description, :frequency, :frequency_unit, :start_date, :notification_date, :notification_email_addresses, :recurring);", reminder)
	if err != nil {
		return fmt.Errorf("error adding new reminder: %w", err)
	}

	return nil
}

func (r *ReminderModel) Update(reminder Reminder) error {
	_, err := r.DB.NamedExec("UPDATE reminders SET title = :title, description = :description, frequency = :frequency, frequency_unit = :frequency_unit, start_date = :start_date, notification_date = :notification_date, notification_email_addresses = :notification_email_addresses, recurring = :recurring WHERE id = :id", reminder)
	if err != nil {
		return fmt.Errorf("error updating reminder: %w", err)
	}

	return nil
}

func (r *ReminderModel) Delete(id int) error {
	_, err := r.DB.Exec("DELETE FROM reminders WHERE id = @p1", id)
	if err != nil {
		return fmt.Errorf("error deleting reminder: %w", err)
	}

	return nil
}
