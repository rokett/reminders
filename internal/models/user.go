package models

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type User struct {
	ID          int       `json:"-"`
	Username    string    `json:"-"`
	Token       string    `json:"-"`
	TokenExpiry time.Time `json:"-" db:"token_expiry"`
	TokenMaxAge int       `json:"-" db:"token_max_age"`
	LastLogin   time.Time `json:"-" db:"last_login"`
	CreatedAt   time.Time `json:"-" db:"created_at"`
	UpdatedAt   time.Time `json:"-" db:"updated_at"`
}

type UserModel struct {
	DB   *sqlx.DB
	User User
}

func (u *UserModel) Upsert(user User) error {
	err := u.DB.Get(&user.ID, "SELECT id FROM users WHERE username = @p1", user.Username)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("unable to check for user: %w", err)
	}

	if user.ID == 0 {
		err = u.create(user)
		if err != nil {
			return fmt.Errorf("unable to create new user record: %w", err)
		}

		return nil
	}

	err = u.update(user)
	if err != nil {
		return fmt.Errorf("unable to update user record: %w", err)
	}

	return nil
}

func (u *UserModel) create(user User) error {
	_, err := u.DB.NamedExec("INSERT INTO users (username, token, token_expiry, token_max_age, last_login) VALUES (:username, :token, :token_expiry, :token_max_age, :last_login);", user)
	if err != nil {
		return fmt.Errorf("error adding new user: %w", err)
	}

	return nil
}

func (u *UserModel) update(user User) error {
	_, err := u.DB.NamedExec("UPDATE users SET token = :token, token_expiry = :token_expiry, token_max_age = :token_max_age, last_login = :last_login WHERE id = :id;", user)
	if err != nil {
		return fmt.Errorf("error updating user: %w", err)
	}

	return nil
}

func (u *UserModel) IsTokenValid(token string) (bool, User, error) {
	var user User

	err := u.DB.Get(&user, "SELECT username, token_expiry FROM users WHERE token = @p1", token)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return false, User{}, nil
	}
	if err != nil {
		return false, User{}, fmt.Errorf("problem checking for session token in database: %w", err)
	}

	if time.Now().UTC().After(user.TokenExpiry) {
		return false, User{}, nil
	}

	return true, user, nil
}
